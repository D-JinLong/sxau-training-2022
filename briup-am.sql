/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : briup-am

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 05/01/2022 17:09:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for am_device
-- ----------------------------
DROP TABLE IF EXISTS `am_device`;
CREATE TABLE `am_device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial_number` varchar(255) NOT NULL COMMENT '设备编号',
  `bind_status` bigint(20) NOT NULL COMMENT '绑定状态',
  `online_status` bigint(20) NOT NULL COMMENT '在线状态',
  `latitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `timer` bigint(20) DEFAULT NULL COMMENT '定时器id',
  `longitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `insert_time` bigint(20) DEFAULT NULL COMMENT '插入时间',
  `video` varchar(255) DEFAULT NULL COMMENT '视频地址',
  `engineer_id` bigint(20) DEFAULT NULL COMMENT '所属工程ID',
  PRIMARY KEY (`id`),
  KEY `enginner_id` (`engineer_id`),
  KEY `name` (`name`),
  CONSTRAINT `am_device_ibfk_1` FOREIGN KEY (`engineer_id`) REFERENCES `am_engineer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of am_device
-- ----------------------------
BEGIN;
INSERT INTO `am_device` VALUES (1, '一号设备', 'DV_20210002', 1, 1, '36.091265', 136, '103.699249', 1639969918785, 'http://8.130.16.184/vv.mov', 2);
INSERT INTO `am_device` VALUES (2, '二号设备', 'DV_20210001', 1, 1, '36.090181', 125, '103.700226', 1639969927152, 'http://8.130.16.184/vv.mov', 2);
INSERT INTO `am_device` VALUES (3, '三号设备', 'DV_20210003', 1, 1, '36.090771', 114, '103.698965', 1639969946535, 'http://8.130.16.184/vv.mov', 1);
INSERT INTO `am_device` VALUES (4, '四号设备', 'DV_20210004', 1, 1, '36.054036', 103, '103.777137', 1639969953486, 'xxx', 4);
INSERT INTO `am_device` VALUES (5, '五号设备', 'DV_20210005', 1, 1, '36.053619', 92, '103.774326', 1639969960815, 'xxx', 4);
COMMIT;

-- ----------------------------
-- Table structure for am_engineer
-- ----------------------------
DROP TABLE IF EXISTS `am_engineer`;
CREATE TABLE `am_engineer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(255) NOT NULL COMMENT '工程编号',
  `name` varchar(255) NOT NULL COMMENT '工程名称',
  `status` varchar(255) DEFAULT NULL COMMENT '工程状态',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `type` varchar(255) NOT NULL COMMENT '工程类型',
  `latitude` varchar(255) DEFAULT NULL COMMENT '经度',
  `longitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `begin_time` bigint(20) DEFAULT NULL COMMENT '工程开始时间',
  `end_time` bigint(20) DEFAULT NULL COMMENT '工程结束时间',
  `charge_id` bigint(20) DEFAULT NULL COMMENT '负责人ID',
  `customer_id` bigint(20) DEFAULT NULL COMMENT '顾客ID',
  PRIMARY KEY (`id`),
  KEY `charge_id` (`charge_id`),
  KEY `customer_id` (`customer_id`),
  KEY `name` (`name`),
  CONSTRAINT `am_engineer_ibfk_1` FOREIGN KEY (`charge_id`) REFERENCES `base_account` (`id`),
  CONSTRAINT `am_engineer_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `base_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of am_engineer
-- ----------------------------
BEGIN;
INSERT INTO `am_engineer` VALUES (1, 'GC_20210001', '山西农大主体育场工程', '监测中', '甘肃省兰州市 undefined', '事业单位', '37.423954', '112.583804', 1639970383926, NULL, NULL, 2, 35);
INSERT INTO `am_engineer` VALUES (2, 'GC_20210002', '太原理工大学一号教学楼工程', '监测中', '山西省 太原市 杏花岭区 迎泽西大街79号', '事业单位', '37.861366', '112.522526', 1639970572966, NULL, NULL, 2, 36);
INSERT INTO `am_engineer` VALUES (3, 'GC_20210003', '中北大学', '未绑定', '山西省 太原市 尖草坪区 学院路3号', '事业单位', '38.015111', '112.447763', 1639988508609, NULL, NULL, 2, 37);
INSERT INTO `am_engineer` VALUES (4, 'GC_20220105', '兰州理工大学', '监测中', '甘肃省 兰州市 七里河区 兰州理工大学', '园区', '36.055211', '103.776515', 1641370790716, NULL, NULL, 1, 40);
COMMIT;

-- ----------------------------
-- Table structure for am_monitor
-- ----------------------------
DROP TABLE IF EXISTS `am_monitor`;
CREATE TABLE `am_monitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `insert_time` bigint(20) DEFAULT NULL COMMENT '数据插入时间',
  `tsp` varchar(255) DEFAULT NULL COMMENT '总悬浮颗粒物',
  `pm10` varchar(255) DEFAULT NULL,
  `pm25` varchar(255) DEFAULT NULL,
  `noise` varchar(255) DEFAULT NULL COMMENT '噪声',
  `temperature` varchar(255) DEFAULT NULL COMMENT '温度',
  `humidity` varchar(255) DEFAULT NULL COMMENT '湿度',
  `wind_speed` varchar(255) DEFAULT NULL COMMENT '风速',
  `wind_direction` varchar(255) DEFAULT NULL COMMENT '风向',
  `engineer_id` bigint(20) DEFAULT NULL COMMENT '所属工程ID',
  `device_id` bigint(20) DEFAULT NULL COMMENT '所属设备ID',
  PRIMARY KEY (`id`),
  KEY `engineer_id` (`engineer_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `am_monitor_ibfk_1` FOREIGN KEY (`engineer_id`) REFERENCES `am_engineer` (`id`),
  CONSTRAINT `am_monitor_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `am_device` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=350 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of am_monitor
-- ----------------------------
BEGIN;
INSERT INTO `am_monitor` VALUES (1, 1641367323428, '62', '107', '59', '50.9', '20.1', '61', '3', '248', 1, 3);
INSERT INTO `am_monitor` VALUES (2, 1641367323448, '39', '111', '44', '33.5', '15.6', '68', '6', '132', 2, 2);
INSERT INTO `am_monitor` VALUES (3, 1641367323452, '35', '111', '68', '67.2', '18', '69', '7', '224', 2, 1);
INSERT INTO `am_monitor` VALUES (4, 1641367383433, '60', '81', '31', '67.3', '15', '38', '6', '286', 1, 3);
INSERT INTO `am_monitor` VALUES (5, 1641367383450, '49', '103', '61', '37.6', '19.7', '45', '7', '313', 2, 2);
INSERT INTO `am_monitor` VALUES (6, 1641367383454, '36', '102', '60', '38.6', '21', '58', '1', '315', 2, 1);
INSERT INTO `am_monitor` VALUES (7, 1641367443435, '67', '88', '70', '59', '20.7', '40', '7', '86', 1, 3);
INSERT INTO `am_monitor` VALUES (8, 1641367443453, '39', '70', '73', '62.5', '15', '53', '5', '344', 2, 2);
INSERT INTO `am_monitor` VALUES (9, 1641367443455, '55', '111', '51', '35', '16.3', '54', '6', '8', 2, 1);
INSERT INTO `am_monitor` VALUES (10, 1641367503443, '51', '80', '41', '45.9', '20.9', '59', '4', '352', 1, 3);
INSERT INTO `am_monitor` VALUES (11, 1641367503455, '33', '115', '80', '30.9', '23.2', '70', '7', '187', 2, 2);
INSERT INTO `am_monitor` VALUES (12, 1641367503459, '51', '88', '67', '62.7', '16.2', '48', '1', '160', 2, 1);
INSERT INTO `am_monitor` VALUES (13, 1641367563445, '45', '80', '65', '35.6', '24.5', '41', '5', '18', 1, 3);
INSERT INTO `am_monitor` VALUES (14, 1641367563458, '30', '91', '79', '46.2', '20', '53', '2', '331', 2, 2);
INSERT INTO `am_monitor` VALUES (15, 1641367563461, '31', '118', '31', '46.3', '22.9', '35', '6', '73', 2, 1);
INSERT INTO `am_monitor` VALUES (16, 1641367623483, '46', '96', '33', '49.1', '18.9', '55', '6', '33', 1, 3);
INSERT INTO `am_monitor` VALUES (17, 1641367623485, '36', '116', '53', '39.5', '25.7', '31', '4', '1', 2, 2);
INSERT INTO `am_monitor` VALUES (18, 1641367623506, '43', '110', '79', '30', '22.2', '63', '3', '25', 2, 1);
INSERT INTO `am_monitor` VALUES (19, 1641367683490, '57', '113', '74', '66.5', '15.5', '54', '6', '257', 1, 3);
INSERT INTO `am_monitor` VALUES (20, 1641367683490, '41', '111', '58', '70.3', '19.5', '51', '4', '76', 2, 2);
INSERT INTO `am_monitor` VALUES (21, 1641367683508, '64', '92', '42', '44.3', '25.5', '62', '1', '191', 2, 1);
INSERT INTO `am_monitor` VALUES (22, 1641367743515, '47', '117', '48', '60.8', '24.4', '59', '3', '92', 2, 1);
INSERT INTO `am_monitor` VALUES (23, 1641367743515, '50', '113', '77', '45.7', '21.5', '42', '2', '225', 2, 2);
INSERT INTO `am_monitor` VALUES (24, 1641367743514, '54', '80', '41', '59.2', '23.5', '59', '3', '44', 1, 3);
INSERT INTO `am_monitor` VALUES (25, 1641367803518, '63', '111', '59', '30.9', '18.2', '63', '4', '99', 1, 3);
INSERT INTO `am_monitor` VALUES (26, 1641367803519, '69', '100', '30', '55.3', '22.6', '50', '7', '235', 2, 2);
INSERT INTO `am_monitor` VALUES (27, 1641367803519, '59', '74', '44', '67.5', '25.3', '65', '6', '83', 2, 1);
INSERT INTO `am_monitor` VALUES (28, 1641367863524, '44', '116', '40', '51.4', '16.8', '43', '5', '174', 1, 3);
INSERT INTO `am_monitor` VALUES (29, 1641367863524, '55', '89', '53', '55.8', '17.6', '37', '6', '248', 2, 2);
INSERT INTO `am_monitor` VALUES (30, 1641367863525, '70', '115', '72', '65', '21.4', '51', '1', '80', 2, 1);
INSERT INTO `am_monitor` VALUES (31, 1641367923529, '54', '75', '72', '33.8', '16.9', '60', '1', '32', 2, 2);
INSERT INTO `am_monitor` VALUES (32, 1641367923528, '62', '72', '66', '57.9', '23', '43', '7', '23', 1, 3);
INSERT INTO `am_monitor` VALUES (33, 1641367923529, '57', '109', '49', '32.2', '17.1', '39', '5', '272', 2, 1);
INSERT INTO `am_monitor` VALUES (34, 1641367983535, '58', '77', '42', '31.9', '23.8', '35', '5', '82', 1, 3);
INSERT INTO `am_monitor` VALUES (35, 1641367983536, '61', '91', '62', '58.3', '23.5', '44', '2', '219', 2, 2);
INSERT INTO `am_monitor` VALUES (36, 1641367983537, '36', '71', '34', '37.4', '18.6', '50', '4', '314', 2, 1);
INSERT INTO `am_monitor` VALUES (37, 1641368043538, '56', '93', '33', '35.6', '25.4', '61', '3', '353', 1, 3);
INSERT INTO `am_monitor` VALUES (38, 1641368043539, '50', '74', '33', '55.2', '15.3', '65', '5', '141', 2, 2);
INSERT INTO `am_monitor` VALUES (39, 1641368043539, '62', '111', '65', '63', '18.1', '40', '1', '358', 2, 1);
INSERT INTO `am_monitor` VALUES (40, 1641368103541, '37', '88', '53', '34.5', '23.2', '46', '6', '53', 2, 2);
INSERT INTO `am_monitor` VALUES (41, 1641368103542, '70', '109', '62', '55.4', '17.9', '47', '3', '124', 2, 1);
INSERT INTO `am_monitor` VALUES (42, 1641368103540, '45', '81', '48', '61.1', '21.5', '49', '2', '177', 1, 3);
INSERT INTO `am_monitor` VALUES (43, 1641368163543, '49', '98', '53', '42.1', '21.8', '51', '6', '203', 1, 3);
INSERT INTO `am_monitor` VALUES (44, 1641368163544, '38', '117', '33', '41.3', '24.6', '61', '3', '238', 2, 2);
INSERT INTO `am_monitor` VALUES (45, 1641368163545, '31', '115', '65', '45.4', '22.6', '39', '3', '285', 2, 1);
INSERT INTO `am_monitor` VALUES (46, 1641368735017, '58', '119', '71', '54.2', '18.4', '53', '5', '348', 2, 2);
INSERT INTO `am_monitor` VALUES (47, 1641368735017, '33', '99', '65', '65.5', '15.7', '46', '1', '347', 2, 1);
INSERT INTO `am_monitor` VALUES (48, 1641368735016, '32', '89', '71', '31.8', '16.3', '68', '6', '158', 1, 3);
INSERT INTO `am_monitor` VALUES (49, 1641369417020, '61', '74', '58', '50.6', '20.1', '63', '1', '107', 1, 3);
INSERT INTO `am_monitor` VALUES (50, 1641369417021, '33', '89', '31', '41.4', '22.8', '59', '6', '353', 2, 2);
INSERT INTO `am_monitor` VALUES (51, 1641369417021, '70', '90', '56', '44.9', '18.1', '41', '3', '299', 2, 1);
INSERT INTO `am_monitor` VALUES (52, 1641369477048, '51', '105', '77', '37.6', '23.9', '66', '7', '113', 1, 3);
INSERT INTO `am_monitor` VALUES (53, 1641369477050, '55', '73', '49', '37.4', '17.6', '65', '6', '255', 2, 1);
INSERT INTO `am_monitor` VALUES (54, 1641369477050, '34', '108', '74', '54.8', '21.8', '65', '5', '261', 2, 2);
INSERT INTO `am_monitor` VALUES (55, 1641369537051, '35', '74', '58', '43.5', '25.2', '53', '7', '39', 1, 3);
INSERT INTO `am_monitor` VALUES (56, 1641369537053, '37', '112', '45', '41.7', '25', '55', '3', '277', 2, 2);
INSERT INTO `am_monitor` VALUES (57, 1641369537054, '67', '78', '55', '43.6', '15.2', '48', '7', '30', 2, 1);
INSERT INTO `am_monitor` VALUES (58, 1641369597055, '46', '78', '72', '51.1', '23.7', '68', '2', '78', 1, 3);
INSERT INTO `am_monitor` VALUES (59, 1641369597057, '46', '112', '32', '67.8', '22', '51', '5', '13', 2, 2);
INSERT INTO `am_monitor` VALUES (60, 1641369597060, '70', '111', '44', '68.2', '23.9', '54', '2', '25', 2, 1);
INSERT INTO `am_monitor` VALUES (61, 1641369718114, '66', '113', '31', '44.7', '16.4', '63', '4', '245', 1, 3);
INSERT INTO `am_monitor` VALUES (62, 1641369718157, '48', '77', '44', '32.9', '20.3', '54', '5', '342', 2, 1);
INSERT INTO `am_monitor` VALUES (63, 1641369718132, '30', '99', '43', '48.1', '16.6', '61', '7', '241', 2, 2);
INSERT INTO `am_monitor` VALUES (64, 1641369778119, '39', '85', '73', '37', '20.7', '35', '1', '29', 1, 3);
INSERT INTO `am_monitor` VALUES (65, 1641369778134, '65', '77', '69', '50.4', '21.9', '35', '1', '87', 2, 2);
INSERT INTO `am_monitor` VALUES (66, 1641369778168, '52', '110', '47', '41.9', '18.1', '59', '5', '130', 2, 1);
INSERT INTO `am_monitor` VALUES (67, 1641369838126, '59', '113', '48', '67.8', '20.2', '65', '3', '276', 1, 3);
INSERT INTO `am_monitor` VALUES (68, 1641369838139, '66', '81', '37', '48.8', '17.2', '62', '5', '128', 2, 2);
INSERT INTO `am_monitor` VALUES (69, 1641369838174, '42', '83', '71', '48.1', '25.4', '63', '7', '92', 2, 1);
INSERT INTO `am_monitor` VALUES (70, 1641369898131, '33', '110', '58', '36.3', '24.3', '38', '2', '281', 1, 3);
INSERT INTO `am_monitor` VALUES (71, 1641369898141, '50', '84', '43', '39.4', '15.1', '31', '4', '337', 2, 2);
INSERT INTO `am_monitor` VALUES (72, 1641369898179, '36', '117', '69', '55.8', '24.4', '68', '4', '111', 2, 1);
INSERT INTO `am_monitor` VALUES (73, 1641369958138, '59', '102', '46', '50.4', '24.8', '56', '5', '237', 1, 3);
INSERT INTO `am_monitor` VALUES (74, 1641369958142, '41', '71', '44', '54.7', '16.9', '44', '6', '291', 2, 2);
INSERT INTO `am_monitor` VALUES (75, 1641369958186, '37', '96', '74', '49.4', '20.8', '47', '6', '332', 2, 1);
INSERT INTO `am_monitor` VALUES (76, 1641370018143, '31', '97', '71', '33.3', '24.9', '58', '3', '192', 1, 3);
INSERT INTO `am_monitor` VALUES (77, 1641370018145, '58', '119', '44', '69.8', '15.5', '52', '1', '93', 2, 2);
INSERT INTO `am_monitor` VALUES (78, 1641370018192, '50', '78', '52', '63.4', '22.6', '50', '6', '247', 2, 1);
INSERT INTO `am_monitor` VALUES (79, 1641370078150, '41', '77', '33', '41.5', '16.9', '69', '3', '58', 1, 3);
INSERT INTO `am_monitor` VALUES (80, 1641370078150, '59', '83', '78', '62.5', '18', '65', '4', '189', 2, 2);
INSERT INTO `am_monitor` VALUES (81, 1641370078198, '53', '84', '30', '49.2', '18', '68', '6', '16', 2, 1);
INSERT INTO `am_monitor` VALUES (82, 1641370138151, '53', '113', '55', '68.8', '19.4', '53', '4', '132', 1, 3);
INSERT INTO `am_monitor` VALUES (83, 1641370138154, '70', '78', '35', '48.5', '15.3', '31', '3', '198', 2, 2);
INSERT INTO `am_monitor` VALUES (84, 1641370138203, '39', '93', '71', '60.2', '22.6', '67', '1', '329', 2, 1);
INSERT INTO `am_monitor` VALUES (85, 1641370198156, '48', '94', '47', '45.6', '22.7', '70', '6', '201', 1, 3);
INSERT INTO `am_monitor` VALUES (86, 1641370198156, '46', '102', '53', '57.1', '16.4', '48', '3', '58', 2, 2);
INSERT INTO `am_monitor` VALUES (87, 1641370198204, '55', '99', '40', '49.1', '25', '38', '5', '292', 2, 1);
INSERT INTO `am_monitor` VALUES (88, 1641370258161, '42', '71', '73', '43.1', '22.8', '49', '3', '351', 1, 3);
INSERT INTO `am_monitor` VALUES (89, 1641370258162, '46', '116', '50', '63.2', '18.6', '52', '1', '82', 2, 2);
INSERT INTO `am_monitor` VALUES (90, 1641370258207, '66', '116', '46', '43.6', '16.3', '70', '3', '330', 2, 1);
INSERT INTO `am_monitor` VALUES (91, 1641370318169, '43', '80', '71', '66.3', '18.7', '31', '1', '244', 1, 3);
INSERT INTO `am_monitor` VALUES (92, 1641370318169, '68', '108', '77', '67', '23.1', '46', '2', '5', 2, 2);
INSERT INTO `am_monitor` VALUES (93, 1641370318213, '64', '95', '71', '63.8', '15.6', '50', '1', '72', 2, 1);
INSERT INTO `am_monitor` VALUES (94, 1641370378174, '43', '78', '46', '52', '20.1', '59', '4', '70', 1, 3);
INSERT INTO `am_monitor` VALUES (95, 1641370378174, '70', '102', '79', '36.8', '16.4', '64', '2', '323', 2, 2);
INSERT INTO `am_monitor` VALUES (96, 1641370378218, '38', '79', '79', '51.8', '20.3', '35', '6', '225', 2, 1);
INSERT INTO `am_monitor` VALUES (97, 1641370440834, '59', '72', '55', '41.6', '17.6', '48', '5', '295', 2, 2);
INSERT INTO `am_monitor` VALUES (98, 1641370440833, '57', '89', '50', '65', '24.5', '47', '7', '272', 1, 3);
INSERT INTO `am_monitor` VALUES (99, 1641370440876, '69', '89', '43', '67.8', '22.4', '49', '7', '292', 2, 1);
INSERT INTO `am_monitor` VALUES (100, 1641370500836, '48', '89', '74', '70.9', '25.8', '40', '5', '229', 1, 3);
INSERT INTO `am_monitor` VALUES (101, 1641370500839, '66', '83', '53', '52.6', '17.5', '32', '7', '161', 2, 2);
INSERT INTO `am_monitor` VALUES (102, 1641370500881, '45', '110', '57', '70.4', '17.9', '44', '7', '265', 2, 1);
INSERT INTO `am_monitor` VALUES (103, 1641370560839, '43', '74', '56', '51.9', '25.4', '53', '2', '360', 1, 3);
INSERT INTO `am_monitor` VALUES (104, 1641370560843, '66', '89', '58', '40.7', '23.7', '39', '4', '146', 2, 2);
INSERT INTO `am_monitor` VALUES (105, 1641370560885, '43', '76', '66', '52.2', '19.3', '61', '1', '0', 2, 1);
INSERT INTO `am_monitor` VALUES (106, 1641370620847, '61', '114', '33', '39.6', '16.6', '56', '4', '15', 1, 3);
INSERT INTO `am_monitor` VALUES (107, 1641370620849, '38', '108', '80', '70.8', '21.2', '47', '4', '254', 2, 2);
INSERT INTO `am_monitor` VALUES (108, 1641370620891, '34', '105', '41', '44.7', '25.1', '55', '2', '262', 2, 1);
INSERT INTO `am_monitor` VALUES (109, 1641370680854, '56', '113', '79', '54.7', '24.9', '38', '7', '102', 1, 3);
INSERT INTO `am_monitor` VALUES (110, 1641370680862, '40', '83', '30', '67.7', '16.7', '31', '6', '261', 2, 2);
INSERT INTO `am_monitor` VALUES (111, 1641370680896, '42', '98', '35', '40.6', '18.9', '49', '1', '180', 2, 1);
INSERT INTO `am_monitor` VALUES (112, 1641370740861, '33', '71', '52', '38.7', '24.4', '40', '7', '66', 1, 3);
INSERT INTO `am_monitor` VALUES (113, 1641370740865, '56', '106', '34', '62.4', '25.1', '39', '6', '337', 2, 2);
INSERT INTO `am_monitor` VALUES (114, 1641370740907, '70', '77', '65', '65.3', '19.2', '38', '3', '185', 2, 1);
INSERT INTO `am_monitor` VALUES (115, 1641370800867, '53', '91', '77', '35.7', '17.5', '55', '6', '358', 2, 2);
INSERT INTO `am_monitor` VALUES (116, 1641370800867, '42', '115', '80', '66.2', '21.4', '67', '5', '250', 1, 3);
INSERT INTO `am_monitor` VALUES (117, 1641370800913, '42', '80', '41', '53.4', '16.9', '45', '7', '79', 2, 1);
INSERT INTO `am_monitor` VALUES (118, 1641370860876, '69', '71', '46', '40.8', '16.6', '53', '1', '52', 1, 3);
INSERT INTO `am_monitor` VALUES (119, 1641370860877, '64', '111', '77', '53.4', '24.8', '58', '3', '336', 2, 2);
INSERT INTO `am_monitor` VALUES (120, 1641370860917, '60', '75', '49', '50.5', '20.9', '37', '6', '335', 2, 1);
INSERT INTO `am_monitor` VALUES (121, 1641370920882, '45', '110', '56', '45.1', '17.9', '68', '3', '30', 1, 3);
INSERT INTO `am_monitor` VALUES (122, 1641370920882, '40', '110', '49', '65.2', '17.6', '63', '3', '37', 2, 2);
INSERT INTO `am_monitor` VALUES (123, 1641370920922, '59', '90', '71', '46.4', '19.3', '50', '7', '26', 2, 1);
INSERT INTO `am_monitor` VALUES (124, 1641370980888, '51', '102', '78', '49.2', '21.8', '49', '6', '160', 1, 3);
INSERT INTO `am_monitor` VALUES (125, 1641370980889, '42', '85', '78', '40.3', '18.2', '61', '6', '246', 2, 2);
INSERT INTO `am_monitor` VALUES (126, 1641370980929, '31', '100', '36', '46.7', '18.2', '54', '6', '321', 2, 1);
INSERT INTO `am_monitor` VALUES (127, 1641371040894, '70', '82', '52', '52.6', '15.3', '65', '1', '205', 2, 2);
INSERT INTO `am_monitor` VALUES (128, 1641371040893, '53', '108', '41', '67.7', '15.8', '34', '4', '92', 1, 3);
INSERT INTO `am_monitor` VALUES (129, 1641371040931, '47', '85', '60', '60.9', '19.4', '43', '3', '45', 2, 1);
INSERT INTO `am_monitor` VALUES (130, 1641371062128, '33', '90', '36', '67.6', '20.1', '50', '7', '136', 4, 5);
INSERT INTO `am_monitor` VALUES (131, 1641371065145, '55', '118', '77', '57', '22.5', '68', '3', '49', 4, 4);
INSERT INTO `am_monitor` VALUES (132, 1641371100900, '43', '96', '70', '56', '20.7', '65', '3', '235', 1, 3);
INSERT INTO `am_monitor` VALUES (133, 1641371100901, '54', '77', '72', '36', '17.4', '60', '6', '134', 2, 2);
INSERT INTO `am_monitor` VALUES (134, 1641371100932, '48', '114', '43', '35.1', '23.3', '45', '7', '160', 2, 1);
INSERT INTO `am_monitor` VALUES (135, 1641371122131, '38', '100', '33', '39.9', '15.9', '69', '3', '74', 4, 5);
INSERT INTO `am_monitor` VALUES (136, 1641371125152, '51', '96', '32', '34.3', '16', '37', '1', '133', 4, 4);
INSERT INTO `am_monitor` VALUES (137, 1641371160904, '52', '73', '34', '31.6', '19.2', '47', '1', '126', 1, 3);
INSERT INTO `am_monitor` VALUES (138, 1641371160905, '60', '98', '57', '57.1', '16.2', '41', '5', '18', 2, 2);
INSERT INTO `am_monitor` VALUES (139, 1641371160937, '62', '73', '55', '37', '25.2', '54', '4', '255', 2, 1);
INSERT INTO `am_monitor` VALUES (140, 1641371182133, '65', '85', '34', '61.9', '25.7', '60', '5', '194', 4, 5);
INSERT INTO `am_monitor` VALUES (141, 1641371185159, '57', '107', '46', '57.9', '20.2', '38', '5', '357', 4, 4);
INSERT INTO `am_monitor` VALUES (142, 1641371220910, '51', '96', '71', '49.4', '16.8', '34', '3', '77', 1, 3);
INSERT INTO `am_monitor` VALUES (143, 1641371220910, '64', '115', '45', '42', '19.5', '68', '4', '292', 2, 2);
INSERT INTO `am_monitor` VALUES (144, 1641371220946, '46', '117', '48', '37.6', '16.3', '43', '3', '74', 2, 1);
INSERT INTO `am_monitor` VALUES (145, 1641371242139, '54', '112', '61', '65', '20.5', '53', '1', '35', 4, 5);
INSERT INTO `am_monitor` VALUES (146, 1641371245166, '65', '101', '58', '31.7', '24.6', '58', '1', '323', 4, 4);
INSERT INTO `am_monitor` VALUES (147, 1641371280915, '61', '80', '36', '70.6', '22.3', '37', '3', '73', 1, 3);
INSERT INTO `am_monitor` VALUES (148, 1641371280917, '31', '71', '78', '64.9', '25.2', '55', '2', '356', 2, 2);
INSERT INTO `am_monitor` VALUES (149, 1641371280951, '69', '97', '52', '54.6', '19.6', '34', '7', '172', 2, 1);
INSERT INTO `am_monitor` VALUES (150, 1641371302139, '35', '111', '52', '49.6', '15.3', '55', '4', '30', 4, 5);
INSERT INTO `am_monitor` VALUES (151, 1641371305165, '45', '113', '55', '53.7', '23', '46', '5', '116', 4, 4);
INSERT INTO `am_monitor` VALUES (152, 1641371340911, '46', '107', '71', '37.4', '17.4', '58', '7', '61', 1, 3);
INSERT INTO `am_monitor` VALUES (153, 1641371340913, '70', '97', '55', '31.4', '16.8', '37', '1', '190', 2, 2);
INSERT INTO `am_monitor` VALUES (154, 1641371340947, '47', '92', '50', '43.4', '21.7', '34', '1', '359', 2, 1);
INSERT INTO `am_monitor` VALUES (155, 1641371362137, '36', '112', '80', '33.9', '21', '62', '3', '198', 4, 5);
INSERT INTO `am_monitor` VALUES (156, 1641371365168, '50', '99', '48', '35.4', '17.4', '51', '4', '29', 4, 4);
INSERT INTO `am_monitor` VALUES (157, 1641371400914, '41', '105', '62', '56.2', '16', '32', '2', '322', 1, 3);
INSERT INTO `am_monitor` VALUES (158, 1641371400915, '31', '111', '71', '47.1', '22.2', '58', '7', '160', 2, 2);
INSERT INTO `am_monitor` VALUES (159, 1641371400956, '40', '101', '77', '61.2', '17.2', '44', '2', '309', 2, 1);
INSERT INTO `am_monitor` VALUES (160, 1641371422140, '44', '117', '38', '51.8', '24.6', '58', '6', '62', 4, 5);
INSERT INTO `am_monitor` VALUES (161, 1641371425173, '63', '92', '75', '66.6', '16.9', '53', '3', '89', 4, 4);
INSERT INTO `am_monitor` VALUES (162, 1641371460921, '40', '108', '72', '42.8', '22.5', '50', '7', '244', 1, 3);
INSERT INTO `am_monitor` VALUES (163, 1641371460921, '62', '92', '30', '67.9', '20.7', '55', '1', '347', 2, 2);
INSERT INTO `am_monitor` VALUES (164, 1641371460963, '41', '88', '49', '50.8', '15.7', '53', '4', '4', 2, 1);
INSERT INTO `am_monitor` VALUES (165, 1641371482144, '57', '101', '47', '37.7', '19.3', '65', '5', '54', 4, 5);
INSERT INTO `am_monitor` VALUES (166, 1641371485178, '39', '86', '56', '51.1', '25.4', '60', '5', '281', 4, 4);
INSERT INTO `am_monitor` VALUES (167, 1641371520923, '68', '76', '46', '34.5', '25.4', '39', '6', '82', 1, 3);
INSERT INTO `am_monitor` VALUES (168, 1641371520925, '53', '89', '31', '42.9', '21.8', '52', '6', '356', 2, 2);
INSERT INTO `am_monitor` VALUES (169, 1641371520969, '46', '72', '35', '47', '19.4', '61', '1', '273', 2, 1);
INSERT INTO `am_monitor` VALUES (170, 1641371542149, '53', '116', '58', '53.1', '17.6', '64', '1', '163', 4, 5);
INSERT INTO `am_monitor` VALUES (171, 1641371545183, '50', '115', '68', '51.7', '18.2', '62', '6', '103', 4, 4);
INSERT INTO `am_monitor` VALUES (172, 1641371580929, '59', '84', '33', '57.1', '15', '40', '6', '246', 2, 2);
INSERT INTO `am_monitor` VALUES (173, 1641371580928, '35', '118', '53', '66.4', '17.7', '41', '6', '61', 1, 3);
INSERT INTO `am_monitor` VALUES (174, 1641371580975, '64', '82', '55', '63.2', '20.4', '55', '2', '83', 2, 1);
INSERT INTO `am_monitor` VALUES (175, 1641371602153, '36', '119', '44', '65.5', '19.2', '30', '4', '286', 4, 5);
INSERT INTO `am_monitor` VALUES (176, 1641371605232, '51', '85', '80', '58.6', '22.6', '51', '5', '248', 4, 4);
INSERT INTO `am_monitor` VALUES (177, 1641371640930, '32', '76', '76', '49.7', '19', '46', '6', '90', 1, 3);
INSERT INTO `am_monitor` VALUES (178, 1641371640931, '62', '84', '30', '31.8', '23.1', '66', '5', '310', 2, 2);
INSERT INTO `am_monitor` VALUES (179, 1641371640982, '34', '84', '64', '36.8', '16.5', '45', '1', '132', 2, 1);
INSERT INTO `am_monitor` VALUES (180, 1641371662159, '34', '86', '44', '62.2', '24.9', '62', '2', '149', 4, 5);
INSERT INTO `am_monitor` VALUES (181, 1641371665234, '44', '83', '75', '70.6', '21.4', '42', '7', '153', 4, 4);
INSERT INTO `am_monitor` VALUES (182, 1641371700936, '49', '113', '52', '63.2', '15.8', '70', '2', '216', 2, 2);
INSERT INTO `am_monitor` VALUES (183, 1641371700935, '48', '107', '36', '64.1', '24.7', '69', '1', '31', 1, 3);
INSERT INTO `am_monitor` VALUES (184, 1641371700992, '52', '85', '68', '64.4', '16.9', '66', '5', '180', 2, 1);
INSERT INTO `am_monitor` VALUES (185, 1641371722162, '56', '73', '50', '33.7', '25.5', '35', '5', '72', 4, 5);
INSERT INTO `am_monitor` VALUES (186, 1641371725243, '51', '70', '74', '61.8', '24.7', '52', '6', '331', 4, 4);
INSERT INTO `am_monitor` VALUES (187, 1641371760938, '50', '119', '50', '65.8', '25.8', '37', '5', '345', 1, 3);
INSERT INTO `am_monitor` VALUES (188, 1641371760939, '47', '85', '34', '30.6', '20.2', '70', '6', '47', 2, 2);
INSERT INTO `am_monitor` VALUES (189, 1641371760998, '45', '104', '42', '59.6', '17.1', '33', '5', '34', 2, 1);
INSERT INTO `am_monitor` VALUES (190, 1641371782166, '56', '114', '51', '39.7', '21.3', '70', '3', '87', 4, 5);
INSERT INTO `am_monitor` VALUES (191, 1641371785251, '55', '90', '79', '45', '15', '57', '2', '329', 4, 4);
INSERT INTO `am_monitor` VALUES (192, 1641371820940, '50', '105', '48', '45.3', '24.8', '61', '4', '94', 1, 3);
INSERT INTO `am_monitor` VALUES (193, 1641371820940, '39', '95', '70', '39.8', '17.1', '30', '4', '351', 2, 2);
INSERT INTO `am_monitor` VALUES (194, 1641371821012, '33', '74', '57', '42.9', '18.6', '53', '2', '63', 2, 1);
INSERT INTO `am_monitor` VALUES (195, 1641371842184, '55', '85', '47', '64.5', '15.1', '64', '4', '59', 4, 5);
INSERT INTO `am_monitor` VALUES (196, 1641371845266, '34', '81', '42', '58', '24.4', '40', '5', '131', 4, 4);
INSERT INTO `am_monitor` VALUES (197, 1641371880945, '61', '100', '77', '40.5', '22.8', '65', '3', '41', 1, 3);
INSERT INTO `am_monitor` VALUES (198, 1641371880946, '46', '98', '63', '68', '17.8', '33', '4', '5', 2, 2);
INSERT INTO `am_monitor` VALUES (199, 1641371881020, '63', '93', '50', '61.8', '25.7', '67', '1', '168', 2, 1);
INSERT INTO `am_monitor` VALUES (200, 1641371902186, '48', '108', '36', '56.7', '15.8', '50', '1', '185', 4, 5);
INSERT INTO `am_monitor` VALUES (201, 1641371905270, '66', '85', '80', '56.1', '17.6', '43', '6', '96', 4, 4);
INSERT INTO `am_monitor` VALUES (202, 1641371940947, '54', '115', '44', '59.9', '24.4', '50', '3', '156', 1, 3);
INSERT INTO `am_monitor` VALUES (203, 1641371940948, '66', '73', '75', '68.3', '20.5', '64', '4', '161', 2, 2);
INSERT INTO `am_monitor` VALUES (204, 1641371941028, '59', '86', '36', '44.3', '18', '58', '4', '342', 2, 1);
INSERT INTO `am_monitor` VALUES (205, 1641371962190, '52', '107', '78', '38.5', '25.3', '44', '5', '110', 4, 5);
INSERT INTO `am_monitor` VALUES (206, 1641371965277, '45', '101', '45', '52.8', '21.4', '60', '1', '67', 4, 4);
INSERT INTO `am_monitor` VALUES (207, 1641372000951, '41', '107', '61', '68', '15.9', '46', '4', '219', 1, 3);
INSERT INTO `am_monitor` VALUES (208, 1641372000951, '59', '73', '72', '65.6', '19.5', '34', '5', '145', 2, 2);
INSERT INTO `am_monitor` VALUES (209, 1641372001032, '38', '97', '38', '39', '21.8', '35', '5', '170', 2, 1);
INSERT INTO `am_monitor` VALUES (210, 1641372022197, '47', '79', '67', '55.9', '18', '39', '1', '196', 4, 5);
INSERT INTO `am_monitor` VALUES (211, 1641372025287, '54', '86', '74', '37.6', '23.8', '65', '4', '43', 4, 4);
INSERT INTO `am_monitor` VALUES (212, 1641372060957, '54', '106', '53', '46.6', '19.1', '70', '3', '118', 1, 3);
INSERT INTO `am_monitor` VALUES (213, 1641372060957, '59', '82', '38', '54', '20.5', '33', '6', '298', 2, 2);
INSERT INTO `am_monitor` VALUES (214, 1641372061039, '66', '105', '35', '50.5', '24', '48', '2', '124', 2, 1);
INSERT INTO `am_monitor` VALUES (215, 1641372082207, '66', '86', '49', '34.6', '21.2', '36', '1', '77', 4, 5);
INSERT INTO `am_monitor` VALUES (216, 1641372085292, '48', '100', '74', '46.7', '15.1', '53', '3', '60', 4, 4);
INSERT INTO `am_monitor` VALUES (217, 1641372120963, '35', '76', '48', '57.8', '23.5', '60', '3', '149', 1, 3);
INSERT INTO `am_monitor` VALUES (218, 1641372120963, '51', '77', '34', '55.2', '22.5', '36', '7', '304', 2, 2);
INSERT INTO `am_monitor` VALUES (219, 1641372121043, '52', '98', '35', '67.4', '17', '63', '4', '128', 2, 1);
INSERT INTO `am_monitor` VALUES (220, 1641372142213, '30', '83', '53', '58', '18.7', '31', '3', '260', 4, 5);
INSERT INTO `am_monitor` VALUES (221, 1641372145299, '30', '79', '44', '34.5', '19.2', '41', '1', '74', 4, 4);
INSERT INTO `am_monitor` VALUES (222, 1641372180967, '63', '75', '75', '68.8', '24.7', '66', '1', '112', 2, 2);
INSERT INTO `am_monitor` VALUES (223, 1641372180966, '52', '88', '70', '66.9', '16.7', '30', '3', '163', 1, 3);
INSERT INTO `am_monitor` VALUES (224, 1641372181049, '58', '75', '33', '48.9', '18.7', '65', '1', '34', 2, 1);
INSERT INTO `am_monitor` VALUES (225, 1641372202137, '66', '116', '49', '37.4', '16.5', '33', '4', '126', 4, 5);
INSERT INTO `am_monitor` VALUES (226, 1641372205224, '32', '104', '68', '50.5', '22.7', '67', '2', '332', 4, 4);
INSERT INTO `am_monitor` VALUES (227, 1641372240915, '53', '83', '48', '47.3', '18.3', '37', '5', '308', 1, 3);
INSERT INTO `am_monitor` VALUES (228, 1641372240916, '32', '117', '60', '51.6', '16', '42', '5', '86', 2, 2);
INSERT INTO `am_monitor` VALUES (229, 1641372240969, '31', '93', '62', '32.9', '19.1', '49', '2', '339', 2, 1);
INSERT INTO `am_monitor` VALUES (230, 1641372262142, '52', '78', '55', '35.7', '23.8', '35', '5', '32', 4, 5);
INSERT INTO `am_monitor` VALUES (231, 1641372265229, '62', '94', '31', '69.3', '17', '55', '2', '352', 4, 4);
INSERT INTO `am_monitor` VALUES (232, 1641372300921, '63', '85', '76', '47.7', '15.6', '65', '6', '173', 1, 3);
INSERT INTO `am_monitor` VALUES (233, 1641372300921, '53', '118', '65', '32.5', '23.3', '41', '6', '206', 2, 2);
INSERT INTO `am_monitor` VALUES (234, 1641372300971, '46', '103', '72', '54', '15.9', '48', '6', '160', 2, 1);
INSERT INTO `am_monitor` VALUES (235, 1641372322146, '69', '100', '66', '38.6', '15.6', '62', '7', '123', 4, 5);
INSERT INTO `am_monitor` VALUES (236, 1641372325234, '68', '113', '61', '57.4', '19.2', '35', '3', '63', 4, 4);
INSERT INTO `am_monitor` VALUES (237, 1641372360925, '56', '111', '66', '63.9', '15.9', '68', '7', '267', 1, 3);
INSERT INTO `am_monitor` VALUES (238, 1641372360925, '67', '83', '71', '63.7', '23.8', '61', '3', '120', 2, 2);
INSERT INTO `am_monitor` VALUES (239, 1641372360973, '50', '88', '72', '67.6', '25.2', '54', '5', '355', 2, 1);
INSERT INTO `am_monitor` VALUES (240, 1641372382149, '46', '92', '74', '67.3', '17.8', '31', '1', '30', 4, 5);
INSERT INTO `am_monitor` VALUES (241, 1641372385240, '63', '107', '64', '47.8', '16.7', '51', '3', '221', 4, 4);
INSERT INTO `am_monitor` VALUES (242, 1641372420930, '69', '107', '47', '54.7', '19.1', '46', '6', '6', 1, 3);
INSERT INTO `am_monitor` VALUES (243, 1641372420930, '55', '113', '51', '64.9', '21.3', '37', '3', '295', 2, 2);
INSERT INTO `am_monitor` VALUES (244, 1641372420979, '70', '77', '56', '47.8', '18.1', '70', '4', '141', 2, 1);
INSERT INTO `am_monitor` VALUES (245, 1641372442152, '62', '75', '71', '43', '20.2', '46', '5', '188', 4, 5);
INSERT INTO `am_monitor` VALUES (246, 1641372445242, '42', '72', '64', '60.3', '24.4', '65', '1', '121', 4, 4);
INSERT INTO `am_monitor` VALUES (247, 1641372480938, '31', '102', '51', '47.2', '19.3', '68', '2', '26', 2, 2);
INSERT INTO `am_monitor` VALUES (248, 1641372480936, '35', '92', '48', '40.6', '20.3', '69', '4', '254', 1, 3);
INSERT INTO `am_monitor` VALUES (249, 1641372480982, '63', '70', '57', '56.3', '17.7', '56', '5', '147', 2, 1);
INSERT INTO `am_monitor` VALUES (250, 1641372502156, '67', '77', '47', '56', '23.3', '33', '6', '313', 4, 5);
INSERT INTO `am_monitor` VALUES (251, 1641372505248, '34', '78', '80', '31.1', '24.9', '46', '2', '89', 4, 4);
INSERT INTO `am_monitor` VALUES (252, 1641372540941, '62', '81', '33', '32.3', '15.3', '35', '2', '60', 2, 2);
INSERT INTO `am_monitor` VALUES (253, 1641372540941, '60', '92', '70', '45.4', '17.4', '58', '4', '164', 1, 3);
INSERT INTO `am_monitor` VALUES (254, 1641372540984, '57', '110', '53', '60.4', '16.2', '59', '7', '352', 2, 1);
INSERT INTO `am_monitor` VALUES (255, 1641372562158, '34', '105', '37', '44.2', '25.4', '34', '4', '265', 4, 5);
INSERT INTO `am_monitor` VALUES (256, 1641372565251, '32', '103', '74', '58.1', '18.3', '68', '3', '229', 4, 4);
INSERT INTO `am_monitor` VALUES (257, 1641372600946, '49', '105', '73', '34.5', '15.5', '56', '6', '161', 1, 3);
INSERT INTO `am_monitor` VALUES (258, 1641372600946, '65', '83', '50', '54.3', '20', '68', '4', '186', 2, 2);
INSERT INTO `am_monitor` VALUES (259, 1641372600988, '64', '117', '36', '47.6', '18.4', '34', '2', '188', 2, 1);
INSERT INTO `am_monitor` VALUES (260, 1641372622163, '36', '107', '79', '48.6', '21.5', '33', '6', '259', 4, 5);
INSERT INTO `am_monitor` VALUES (261, 1641372625253, '66', '110', '79', '33.3', '24.8', '55', '5', '324', 4, 4);
INSERT INTO `am_monitor` VALUES (262, 1641372660951, '42', '111', '38', '39.5', '20.3', '68', '3', '17', 1, 3);
INSERT INTO `am_monitor` VALUES (263, 1641372660951, '54', '101', '80', '49', '23.4', '43', '3', '260', 2, 2);
INSERT INTO `am_monitor` VALUES (264, 1641372660993, '42', '118', '64', '41.6', '17.3', '68', '3', '89', 2, 1);
INSERT INTO `am_monitor` VALUES (265, 1641372682164, '67', '108', '58', '53.6', '15.8', '57', '6', '259', 4, 5);
INSERT INTO `am_monitor` VALUES (266, 1641372685256, '35', '116', '78', '64.3', '19.9', '50', '6', '176', 4, 4);
INSERT INTO `am_monitor` VALUES (267, 1641372720952, '31', '100', '44', '37.1', '15.5', '37', '3', '40', 1, 3);
INSERT INTO `am_monitor` VALUES (268, 1641372720952, '41', '118', '33', '52.4', '19.6', '34', '6', '288', 2, 2);
INSERT INTO `am_monitor` VALUES (269, 1641372720994, '40', '75', '35', '49.5', '15.4', '55', '1', '274', 2, 1);
INSERT INTO `am_monitor` VALUES (270, 1641372742167, '46', '93', '74', '35.3', '19.4', '54', '7', '152', 4, 5);
INSERT INTO `am_monitor` VALUES (271, 1641372745261, '53', '91', '57', '69.2', '15.8', '64', '5', '147', 4, 4);
INSERT INTO `am_monitor` VALUES (272, 1641372780952, '65', '83', '66', '51.3', '21', '60', '6', '317', 1, 3);
INSERT INTO `am_monitor` VALUES (273, 1641372780953, '34', '91', '30', '64', '22.3', '64', '6', '104', 2, 2);
INSERT INTO `am_monitor` VALUES (274, 1641372780996, '32', '86', '80', '65.6', '21.4', '53', '4', '254', 2, 1);
INSERT INTO `am_monitor` VALUES (275, 1641372802172, '58', '86', '63', '44.8', '16.2', '52', '1', '138', 4, 5);
INSERT INTO `am_monitor` VALUES (276, 1641372805264, '38', '70', '41', '60.8', '22.6', '58', '4', '116', 4, 4);
INSERT INTO `am_monitor` VALUES (277, 1641372840957, '48', '83', '41', '59.6', '22.9', '52', '6', '86', 1, 3);
INSERT INTO `am_monitor` VALUES (278, 1641372840958, '36', '101', '78', '70.9', '23.7', '47', '5', '100', 2, 2);
INSERT INTO `am_monitor` VALUES (279, 1641372841000, '32', '81', '71', '59.4', '17', '55', '4', '238', 2, 1);
INSERT INTO `am_monitor` VALUES (280, 1641372862174, '56', '79', '42', '31.9', '16.7', '42', '5', '319', 4, 5);
INSERT INTO `am_monitor` VALUES (281, 1641372865279, '53', '100', '49', '50', '22.6', '49', '4', '173', 4, 4);
INSERT INTO `am_monitor` VALUES (282, 1641372900958, '34', '101', '53', '34.3', '24.5', '56', '3', '222', 1, 3);
INSERT INTO `am_monitor` VALUES (283, 1641372900959, '34', '105', '34', '37.6', '24.5', '54', '5', '215', 2, 2);
INSERT INTO `am_monitor` VALUES (284, 1641372901004, '56', '75', '53', '58', '15.7', '52', '7', '51', 2, 1);
INSERT INTO `am_monitor` VALUES (285, 1641372922181, '46', '114', '31', '47.3', '20.8', '69', '1', '54', 4, 5);
INSERT INTO `am_monitor` VALUES (286, 1641372925279, '49', '88', '62', '39.2', '25.5', '68', '4', '87', 4, 4);
INSERT INTO `am_monitor` VALUES (287, 1641372960961, '51', '119', '56', '32.7', '25', '70', '7', '192', 1, 3);
INSERT INTO `am_monitor` VALUES (288, 1641372960962, '36', '81', '34', '58.9', '22.9', '63', '5', '185', 2, 2);
INSERT INTO `am_monitor` VALUES (289, 1641372961008, '45', '104', '50', '65.7', '23.7', '36', '1', '186', 2, 1);
INSERT INTO `am_monitor` VALUES (290, 1641373038002, '63', '78', '52', '66.5', '21.2', '46', '5', '235', 4, 5);
INSERT INTO `am_monitor` VALUES (291, 1641373038012, '65', '89', '43', '67.1', '24.1', '52', '3', '6', 1, 3);
INSERT INTO `am_monitor` VALUES (292, 1641373038015, '66', '119', '30', '55.3', '18.3', '30', '3', '19', 2, 2);
INSERT INTO `am_monitor` VALUES (293, 1641373038006, '56', '77', '67', '70.3', '23.4', '44', '1', '48', 4, 4);
INSERT INTO `am_monitor` VALUES (294, 1641373038017, '50', '107', '76', '58.4', '22.5', '58', '7', '291', 2, 1);
INSERT INTO `am_monitor` VALUES (295, 1641373098001, '44', '77', '54', '52.3', '19.4', '52', '1', '111', 4, 5);
INSERT INTO `am_monitor` VALUES (296, 1641373098003, '54', '92', '80', '55.7', '15.1', '45', '2', '169', 4, 4);
INSERT INTO `am_monitor` VALUES (297, 1641373098010, '41', '74', '41', '31.3', '18.6', '41', '6', '95', 1, 3);
INSERT INTO `am_monitor` VALUES (298, 1641373098013, '68', '118', '51', '37.5', '18.7', '48', '6', '44', 2, 2);
INSERT INTO `am_monitor` VALUES (299, 1641373098015, '39', '96', '42', '70.3', '16.6', '33', '4', '111', 2, 1);
INSERT INTO `am_monitor` VALUES (300, 1641373157992, '64', '96', '69', '41.9', '17.9', '50', '6', '0', 4, 5);
INSERT INTO `am_monitor` VALUES (301, 1641373157995, '57', '102', '73', '50.2', '18.9', '31', '6', '31', 4, 4);
INSERT INTO `am_monitor` VALUES (302, 1641373158001, '56', '84', '48', '51.7', '23.3', '42', '2', '237', 1, 3);
INSERT INTO `am_monitor` VALUES (303, 1641373158004, '62', '100', '66', '42.1', '17.2', '34', '3', '110', 2, 2);
INSERT INTO `am_monitor` VALUES (304, 1641373158007, '48', '113', '55', '48.1', '21.4', '68', '2', '326', 2, 1);
INSERT INTO `am_monitor` VALUES (305, 1641373217995, '60', '120', '38', '46', '18.5', '31', '3', '275', 4, 5);
INSERT INTO `am_monitor` VALUES (306, 1641373217995, '35', '106', '50', '49.3', '18.6', '69', '6', '124', 4, 4);
INSERT INTO `am_monitor` VALUES (307, 1641373218002, '52', '100', '40', '46', '15.1', '37', '2', '242', 1, 3);
INSERT INTO `am_monitor` VALUES (308, 1641373218005, '40', '97', '59', '49.5', '19.9', '61', '2', '306', 2, 2);
INSERT INTO `am_monitor` VALUES (309, 1641373218007, '32', '109', '65', '47.4', '21.5', '55', '6', '59', 2, 1);
INSERT INTO `am_monitor` VALUES (310, 1641373299467, '41', '79', '73', '67.8', '17.4', '38', '7', '354', 4, 5);
INSERT INTO `am_monitor` VALUES (311, 1641373299473, '43', '81', '74', '57', '16.3', '63', '7', '16', 4, 4);
INSERT INTO `am_monitor` VALUES (312, 1641373299475, '44', '118', '41', '58.4', '17.7', '47', '2', '126', 1, 3);
INSERT INTO `am_monitor` VALUES (313, 1641373299485, '49', '90', '75', '63.2', '21.6', '31', '7', '180', 2, 2);
INSERT INTO `am_monitor` VALUES (314, 1641373299488, '43', '87', '62', '37.2', '22.8', '57', '6', '102', 2, 1);
INSERT INTO `am_monitor` VALUES (315, 1641373359470, '34', '73', '31', '58.8', '21', '42', '7', '20', 4, 5);
INSERT INTO `am_monitor` VALUES (316, 1641373359477, '66', '105', '50', '60.1', '23.8', '31', '1', '260', 1, 3);
INSERT INTO `am_monitor` VALUES (317, 1641373359474, '49', '81', '54', '35.8', '23.8', '42', '6', '96', 4, 4);
INSERT INTO `am_monitor` VALUES (318, 1641373359487, '59', '79', '42', '57.1', '22.4', '63', '3', '262', 2, 2);
INSERT INTO `am_monitor` VALUES (319, 1641373359488, '62', '73', '31', '52.6', '19.8', '46', '5', '319', 2, 1);
INSERT INTO `am_monitor` VALUES (320, 1641373456515, '34', '93', '52', '59.9', '21.2', '47', '5', '212', 4, 5);
INSERT INTO `am_monitor` VALUES (321, 1641373456531, '35', '100', '32', '47.4', '20', '41', '5', '185', 4, 4);
INSERT INTO `am_monitor` VALUES (322, 1641373456542, '39', '75', '66', '61', '19.4', '70', '2', '126', 2, 2);
INSERT INTO `am_monitor` VALUES (323, 1641373456533, '38', '83', '44', '32.5', '25.9', '66', '4', '300', 1, 3);
INSERT INTO `am_monitor` VALUES (324, 1641373456545, '51', '90', '75', '61.8', '15.8', '66', '2', '185', 2, 1);
INSERT INTO `am_monitor` VALUES (325, 1641373543270, '34', '77', '63', '57.3', '19.8', '61', '6', '282', 4, 5);
INSERT INTO `am_monitor` VALUES (326, 1641373543286, '69', '70', '68', '32.6', '17.8', '52', '7', '342', 4, 4);
INSERT INTO `am_monitor` VALUES (327, 1641373543289, '68', '90', '62', '68.7', '20.5', '46', '4', '279', 1, 3);
INSERT INTO `am_monitor` VALUES (328, 1641373543295, '67', '100', '54', '39.5', '17.1', '39', '7', '359', 2, 2);
INSERT INTO `am_monitor` VALUES (329, 1641373543299, '45', '101', '56', '46.7', '21.1', '64', '7', '270', 2, 1);
INSERT INTO `am_monitor` VALUES (330, 1641373603273, '47', '78', '43', '62.2', '20.5', '30', '2', '296', 4, 5);
INSERT INTO `am_monitor` VALUES (331, 1641373603289, '57', '96', '64', '36.8', '18.6', '38', '7', '261', 4, 4);
INSERT INTO `am_monitor` VALUES (332, 1641373603290, '65', '97', '37', '52.4', '17.9', '60', '6', '13', 1, 3);
INSERT INTO `am_monitor` VALUES (333, 1641373603295, '56', '107', '65', '32.2', '23.8', '68', '6', '329', 2, 2);
INSERT INTO `am_monitor` VALUES (334, 1641373603300, '34', '71', '60', '34.4', '24.7', '49', '6', '267', 2, 1);
INSERT INTO `am_monitor` VALUES (335, 1641373663274, '32', '82', '60', '68.7', '16.4', '50', '2', '130', 4, 5);
INSERT INTO `am_monitor` VALUES (336, 1641373663291, '32', '101', '32', '62.5', '16.6', '45', '2', '94', 4, 4);
INSERT INTO `am_monitor` VALUES (337, 1641373663292, '52', '93', '53', '40.9', '19.7', '69', '6', '342', 1, 3);
INSERT INTO `am_monitor` VALUES (338, 1641373663295, '46', '83', '68', '69', '22.6', '55', '2', '286', 2, 2);
INSERT INTO `am_monitor` VALUES (339, 1641373663301, '52', '82', '51', '40.1', '22.6', '37', '1', '105', 2, 1);
INSERT INTO `am_monitor` VALUES (340, 1641373723276, '65', '101', '72', '70', '21.3', '40', '3', '17', 4, 5);
INSERT INTO `am_monitor` VALUES (341, 1641373723293, '39', '113', '35', '42.8', '18.8', '47', '2', '255', 4, 4);
INSERT INTO `am_monitor` VALUES (342, 1641373723294, '52', '116', '66', '59.7', '25.7', '56', '5', '288', 1, 3);
INSERT INTO `am_monitor` VALUES (343, 1641373723296, '57', '100', '40', '50.1', '16.3', '47', '3', '324', 2, 2);
INSERT INTO `am_monitor` VALUES (344, 1641373723302, '70', '92', '33', '65.7', '15.7', '30', '4', '33', 2, 1);
INSERT INTO `am_monitor` VALUES (345, 1641373783277, '42', '100', '58', '42.7', '16.4', '43', '6', '354', 4, 5);
INSERT INTO `am_monitor` VALUES (346, 1641373783295, '44', '87', '68', '65.5', '17.8', '59', '3', '300', 4, 4);
INSERT INTO `am_monitor` VALUES (347, 1641373783295, '60', '109', '54', '51', '25.7', '49', '3', '210', 1, 3);
INSERT INTO `am_monitor` VALUES (348, 1641373783298, '34', '120', '74', '50.4', '21.7', '31', '3', '347', 2, 2);
INSERT INTO `am_monitor` VALUES (349, 1641373783303, '61', '107', '42', '54.5', '16.2', '47', '4', '106', 2, 1);
COMMIT;

-- ----------------------------
-- Table structure for am_workorder
-- ----------------------------
DROP TABLE IF EXISTS `am_workorder`;
CREATE TABLE `am_workorder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '工单类型',
  `status` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '工单状态',
  `create_time` bigint(20) NOT NULL,
  `bill_why` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '工单生成原因',
  `finish_time` bigint(20) DEFAULT NULL COMMENT '工单处理完毕时间',
  `finish_note` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '工单处理完毕备注',
  `photo1` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '结束工单照片1',
  `photo2` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '结束工单照片2',
  `photo3` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '结束工单照片3',
  `engineer_id` bigint(20) DEFAULT NULL COMMENT '工程ID',
  `device_id` bigint(20) DEFAULT NULL COMMENT '设备ID',
  `account_id` bigint(20) DEFAULT NULL COMMENT '工人账号ID',
  `p1` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '生成工单照片1',
  `p2` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '生成工单照片2',
  `p3` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '生成工单照片3',
  PRIMARY KEY (`id`),
  KEY `engineer_id` (`engineer_id`),
  KEY `device_id` (`device_id`),
  KEY `account_id` (`account_id`),
  CONSTRAINT `am_workorder_ibfk_1` FOREIGN KEY (`engineer_id`) REFERENCES `am_engineer` (`id`),
  CONSTRAINT `am_workorder_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `am_device` (`id`),
  CONSTRAINT `am_workorder_ibfk_3` FOREIGN KEY (`account_id`) REFERENCES `base_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of am_workorder
-- ----------------------------
BEGIN;
INSERT INTO `am_workorder` VALUES (1, '安装', '已完成', 1639972025041, NULL, 1640053594458, NULL, 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWHBO1CAf42mAABq2BdUUMI627.jpg', 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWHBO1KAOdjrAAAe9thEO8Y251.jpg', 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWHBO1WAetE6AABMxFgBI4s958.jpg', 2, 1, 38, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (2, '安装', '进行中', 1639972578005, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2, 1, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (3, '安装', '进行中', 1639987536424, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3, 1, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (4, '维修', '进行中', 1639989242356, '外壳损坏', NULL, NULL, NULL, NULL, NULL, 1, 3, 1, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (5, '维修', '已完成', 1640051646918, '无法采集数据', 1640053703574, NULL, 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWHBO8CAMM_2AABq2BdUUMI668.jpg', 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWHBO8OAKN8RAAAe9thEO8Y816.jpg', 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWHBO8WAPS7oAABMxFgBI4s353.jpg', 2, 1, 38, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (6, '维修', '进行中', 1640053966052, '摄像头故障', NULL, NULL, NULL, NULL, NULL, 1, 3, 38, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (7, '安装', '待接单', 1641370916987, NULL, NULL, NULL, NULL, NULL, NULL, 4, 4, NULL, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (8, '安装', '待接单', 1641370945252, NULL, NULL, NULL, NULL, NULL, NULL, 4, 5, NULL, NULL, NULL, NULL);
INSERT INTO `am_workorder` VALUES (9, '维修', '已完成', 1641371507458, '摄像头损坏', 1641371611082, NULL, 'http://121.199.29.84:8888/group1/M00/00/C3/rBD-SWHVV9KAYY03AABSRSVX5l4264.jpg', 'http://121.199.29.84:8888/group1/M00/00/C3/rBD-SWHVV9aAa-FGAABSRSVX5l4821.jpg', 'http://121.199.29.84:8888/group1/M00/00/C3/rBD-SWHVV9mAf8JcAABSRSVX5l4221.jpg', 4, 4, 38, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for base_account
-- ----------------------------
DROP TABLE IF EXISTS `base_account`;
CREATE TABLE `base_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL COMMENT '性别',
  `birth` bigint(20) DEFAULT NULL,
  `user_face` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `register_time` bigint(20) NOT NULL COMMENT '注册时间',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`),
  KEY `base_account_ibfk_1` (`role_id`),
  CONSTRAINT `base_account_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `base_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_account
-- ----------------------------
BEGIN;
INSERT INTO `base_account` VALUES (1, 'admin', '123321', '张明', '13466779980', 'male', 1638350673589, 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWG3Zr-AA3PUAAAyUQ2W3FM62.jpeg', '正常', 1638344609075, 1);
INSERT INTO `base_account` VALUES (2, 'lerry', '123321', '李丽', '13566457783', 'male', 1638344609075, 'http://121.199.29.84:8888/group1/M00/00/13/rBD-SV-gGM6Ad_nNAALz1utokUc055.jpg', '正常', 1638350673589, 3);
INSERT INTO `base_account` VALUES (12, '康康', '123321', '康康1', '155645634326', 'male', 1639324800000, 'http://121.199.29.84:8888/group1/M00/00/9F/rBD-SWGxnIuAWUxOAAC1dUCi6FI709.png', '正常', 1638786597243, 4);
INSERT INTO `base_account` VALUES (17, 'moon', '123321', '李丽丽', '13466457789', 'male', 1638501668835, 'http://121.199.29.84:8888/group1/M00/00/9F/rBD-SWGxuE-AEUI6AADskB4isfQ992.jpg', '正常', 1638788311170, 4);
INSERT INTO `base_account` VALUES (21, 'Terry', '123321', '张三', '13466457789', 'female', 1638501668835, 'http://121.199.29.84:8888/group1/M00/00/9F/rBD-SWGxnHqAagWoAAEQLTBSqH4401.png', '正常', 1638950982465, 3);
INSERT INTO `base_account` VALUES (23, 'customer1', '123321', '吴老板', '13466457789', 'male', 1640620800000, 'http://121.199.29.84:8888/group1/M00/00/9F/rBD-SWGx0XyATzzbAAJKhFZ5wpA949.png', '正常', 1638959211571, 4);
INSERT INTO `base_account` VALUES (26, 'kangkang', '123321', '康康2', '15536332698', 'male', 1639411200000, 'http://xxx/xxxx111.jpg', '正常', 1639099623706, 1);
INSERT INTO `base_account` VALUES (27, 'larry', '123321', '李阳', '13466457789', 'male', 1638501668835, 'http://xxx/xxx.jgp', '正常', 1639103357218, 1);
INSERT INTO `base_account` VALUES (28, 'jacky', '123321', '王力宏', '15677890345', 'male', 1639117644649, 'http://xxx/xxxx.jpg', '正常', 1639117644649, 3);
INSERT INTO `base_account` VALUES (29, '张三', '123321', '张三', '13465789988', NULL, NULL, NULL, '正常', 1639126660575, 4);
INSERT INTO `base_account` VALUES (30, '王大山', '123321', '王大山', '13455667788', NULL, NULL, NULL, '正常', 1639126963201, 4);
INSERT INTO `base_account` VALUES (32, '陈乔年', '123321', '陈乔年', '18801010202', NULL, NULL, NULL, '正常', 1639386173746, 4);
INSERT INTO `base_account` VALUES (33, '赵刚', '123321', '赵刚', '15833467809', NULL, NULL, NULL, '正常', 1639398710343, 4);
INSERT INTO `base_account` VALUES (34, '张三', '123321', '张三', '18812349901', NULL, NULL, NULL, '正常', 1639882283477, 4);
INSERT INTO `base_account` VALUES (35, '张小龙', '123321', '张小龙', '18812340001', NULL, NULL, NULL, '正常', 1639970383923, 4);
INSERT INTO `base_account` VALUES (36, '张铭', '123321', '张铭', '18812340002', NULL, NULL, NULL, '正常', 1639970572964, 4);
INSERT INTO `base_account` VALUES (37, '李璐', '123321', '李璐', '18812340003', NULL, NULL, NULL, '正常', 1639988508606, 4);
INSERT INTO `base_account` VALUES (38, 'worker', '123321', '工人一号', '18812344400', 'male', 1640016000000, 'http://121.199.29.84:8888/group1/M00/00/A1/rBD-SWHBl02ANMQ2AABMxFgBI4s651.jpg', '正常', 1640049064521, 13);
INSERT INTO `base_account` VALUES (39, 'worker2', '123321', '工人2号', '18812340008', 'male', 944582400000, NULL, '正常', 1640076068779, 13);
INSERT INTO `base_account` VALUES (40, '兰州理工大学校长', '123321', '兰州理工大学校长', '18811112222', NULL, NULL, NULL, '正常', 1641370790702, 4);
COMMIT;

-- ----------------------------
-- Table structure for base_config
-- ----------------------------
DROP TABLE IF EXISTS `base_config`;
CREATE TABLE `base_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '配置名称',
  `value` varchar(255) NOT NULL,
  `introduce` varchar(255) DEFAULT NULL COMMENT '简介',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_config
-- ----------------------------
BEGIN;
INSERT INTO `base_config` VALUES (1, 'logo', 'http://121.199.29.84:8888/group1/M00/00/9F/rBD-SWGwg-yADOPOAABrMGeCo1A040.jpg', '系统logo');
INSERT INTO `base_config` VALUES (2, 'name', '智慧城市环境监测系统', '项目名称');
INSERT INTO `base_config` VALUES (3, 'copyright', '上海杰普软件科技有限公司 沪ICP备07000318号-1', '版权');
INSERT INTO `base_config` VALUES (5, 'address', '江苏省苏州市昆山市巴城镇学院路828号', '地址');
COMMIT;

-- ----------------------------
-- Table structure for base_privilege
-- ----------------------------
DROP TABLE IF EXISTS `base_privilege`;
CREATE TABLE `base_privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `route` varchar(255) NOT NULL,
  `route_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `hidden` int(255) DEFAULT NULL,
  `num` int(11) DEFAULT NULL COMMENT '序号',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父栏目ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_privilege
-- ----------------------------
BEGIN;
INSERT INTO `base_privilege` VALUES (6, '角色权限', '', '/sys/', 'Role', 'menu', 'icon_5', 0, 1, NULL);
INSERT INTO `base_privilege` VALUES (7, '权限管理', '', '/sys/privilege/List', '', 'menu', '', 0, 0, 6);
INSERT INTO `base_privilege` VALUES (8, '用户管理', '', '/sys/user/List', '0', 'menu', '', 0, 0, 6);
INSERT INTO `base_privilege` VALUES (9, '角色管理', '', '/sys/role/List', '', 'menu', '', 0, 0, 6);
INSERT INTO `base_privilege` VALUES (13, '工程设备', '', '/am/engineer', '', 'menu', 'icon_2', 0, 9, NULL);
INSERT INTO `base_privilege` VALUES (14, '工程管理', NULL, '/am/engineer/List', NULL, 'menu', NULL, 0, 0, 13);
INSERT INTO `base_privilege` VALUES (15, '设备管理', NULL, '/am/device/List', NULL, 'menu', NULL, 0, 0, 13);
INSERT INTO `base_privilege` VALUES (17, '监测数据', NULL, '/am/monitor', NULL, 'menu', 'icon_4', 0, 7, NULL);
INSERT INTO `base_privilege` VALUES (19, '实时监测', NULL, '/am/monitor/RealTime', NULL, 'menu', '0', 0, 0, 17);
INSERT INTO `base_privilege` VALUES (20, '监测月报', NULL, '/am/monitor/Monthly', NULL, 'menu', '0', 0, 0, 17);
INSERT INTO `base_privilege` VALUES (23, '历史数据', '', '/am/monitor/History', '', 'menu', '0', 0, 0, 17);
INSERT INTO `base_privilege` VALUES (25, '监测视频', '', '/am/monitor/Video', '', 'menu', '0', 0, 0, 17);
INSERT INTO `base_privilege` VALUES (26, '监测点信息', '', '/am/monitor/PointInfo', '', 'menu', '0', 0, 0, 17);
INSERT INTO `base_privilege` VALUES (27, '工程详情', NULL, '/am/engineer/Details', NULL, 'menu', NULL, 1, NULL, 13);
INSERT INTO `base_privilege` VALUES (28, '基础配置', NULL, '/sys/config', NULL, 'menu', 'icon_3', 0, 0, NULL);
INSERT INTO `base_privilege` VALUES (29, '系统配置', NULL, '/sys/config/List', NULL, 'menu', NULL, 0, NULL, 28);
INSERT INTO `base_privilege` VALUES (30, '个人信息', NULL, '/sys/user/Personal', NULL, 'menu', NULL, 0, NULL, 28);
INSERT INTO `base_privilege` VALUES (31, '工单管理', NULL, '/am/order', NULL, 'menu', 'icon_8', 0, 8, NULL);
INSERT INTO `base_privilege` VALUES (32, '工单列表', NULL, '/am/order/List', NULL, 'menu', NULL, 0, NULL, 31);
INSERT INTO `base_privilege` VALUES (33, '我要报修', NULL, '/am/order/Repairs', NULL, 'menu', NULL, 0, NULL, 31);
INSERT INTO `base_privilege` VALUES (35, '工程管理', NULL, '/pds/engineer/List', NULL, 'menu', NULL, 0, NULL, 34);
INSERT INTO `base_privilege` VALUES (36, '设备管理', NULL, '/pds/device/List', NULL, 'menu', NULL, 0, NULL, 34);
INSERT INTO `base_privilege` VALUES (38, '工程详情', NULL, '/pds/engineer/Details', NULL, 'menu', NULL, 1, NULL, 34);
INSERT INTO `base_privilege` VALUES (39, '用户详情', NULL, '/sys/user/Details', NULL, 'menu', NULL, 1, NULL, 6);
INSERT INTO `base_privilege` VALUES (42, '工单详情', NULL, '/am/order/Details', NULL, 'menu', NULL, 1, NULL, 31);
INSERT INTO `base_privilege` VALUES (43, '学生选课', NULL, '/sc', NULL, 'menu', 'icon_18', 0, 5, NULL);
INSERT INTO `base_privilege` VALUES (44, '学生管理', NULL, '/sc/student/List', NULL, 'menu', NULL, 0, NULL, 43);
INSERT INTO `base_privilege` VALUES (45, '课程管理', NULL, '/sc/course/List', NULL, 'menu', NULL, 0, NULL, 43);
INSERT INTO `base_privilege` VALUES (46, '学生档案', NULL, '/sc/student/Details', NULL, 'menu', NULL, 1, NULL, 43);
COMMIT;

-- ----------------------------
-- Table structure for base_role
-- ----------------------------
DROP TABLE IF EXISTS `base_role`;
CREATE TABLE `base_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_role
-- ----------------------------
BEGIN;
INSERT INTO `base_role` VALUES (1, '管理员');
INSERT INTO `base_role` VALUES (3, '区域经理');
INSERT INTO `base_role` VALUES (4, '客户');
INSERT INTO `base_role` VALUES (13, '工人');
COMMIT;

-- ----------------------------
-- Table structure for base_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `base_role_privilege`;
CREATE TABLE `base_role_privilege` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `privilege_id` bigint(20) NOT NULL COMMENT '权限ID',
  PRIMARY KEY (`id`),
  KEY `base_role_privilege_ibfk_3` (`role_id`),
  KEY `base_role_privilege_ibfk_4` (`privilege_id`),
  CONSTRAINT `base_role_privilege_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `base_role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `base_role_privilege_ibfk_4` FOREIGN KEY (`privilege_id`) REFERENCES `base_privilege` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_role_privilege
-- ----------------------------
BEGIN;
INSERT INTO `base_role_privilege` VALUES (76, 3, 13);
INSERT INTO `base_role_privilege` VALUES (77, 3, 14);
INSERT INTO `base_role_privilege` VALUES (78, 3, 15);
INSERT INTO `base_role_privilege` VALUES (80, 3, 27);
INSERT INTO `base_role_privilege` VALUES (387, 4, 23);
INSERT INTO `base_role_privilege` VALUES (388, 4, 17);
INSERT INTO `base_role_privilege` VALUES (390, 4, 19);
INSERT INTO `base_role_privilege` VALUES (391, 4, 20);
INSERT INTO `base_role_privilege` VALUES (392, 4, 25);
INSERT INTO `base_role_privilege` VALUES (393, 4, 26);
INSERT INTO `base_role_privilege` VALUES (394, 4, 28);
INSERT INTO `base_role_privilege` VALUES (395, 4, 29);
INSERT INTO `base_role_privilege` VALUES (396, 4, 30);
INSERT INTO `base_role_privilege` VALUES (407, 13, 33);
INSERT INTO `base_role_privilege` VALUES (408, 13, 28);
INSERT INTO `base_role_privilege` VALUES (409, 13, 31);
INSERT INTO `base_role_privilege` VALUES (410, 13, 29);
INSERT INTO `base_role_privilege` VALUES (411, 13, 32);
INSERT INTO `base_role_privilege` VALUES (495, 1, 13);
INSERT INTO `base_role_privilege` VALUES (496, 1, 6);
INSERT INTO `base_role_privilege` VALUES (497, 1, 14);
INSERT INTO `base_role_privilege` VALUES (498, 1, 15);
INSERT INTO `base_role_privilege` VALUES (499, 1, 27);
INSERT INTO `base_role_privilege` VALUES (500, 1, 7);
INSERT INTO `base_role_privilege` VALUES (501, 1, 8);
INSERT INTO `base_role_privilege` VALUES (502, 1, 9);
INSERT INTO `base_role_privilege` VALUES (503, 1, 39);
INSERT INTO `base_role_privilege` VALUES (504, 1, 17);
INSERT INTO `base_role_privilege` VALUES (505, 1, 19);
INSERT INTO `base_role_privilege` VALUES (506, 1, 20);
INSERT INTO `base_role_privilege` VALUES (507, 1, 23);
INSERT INTO `base_role_privilege` VALUES (508, 1, 25);
INSERT INTO `base_role_privilege` VALUES (509, 1, 26);
INSERT INTO `base_role_privilege` VALUES (510, 1, 28);
INSERT INTO `base_role_privilege` VALUES (511, 1, 29);
INSERT INTO `base_role_privilege` VALUES (512, 1, 32);
INSERT INTO `base_role_privilege` VALUES (513, 1, 30);
INSERT INTO `base_role_privilege` VALUES (514, 1, 33);
INSERT INTO `base_role_privilege` VALUES (515, 1, 31);
INSERT INTO `base_role_privilege` VALUES (516, 1, 42);
INSERT INTO `base_role_privilege` VALUES (517, 1, 44);
INSERT INTO `base_role_privilege` VALUES (518, 1, 43);
INSERT INTO `base_role_privilege` VALUES (519, 1, 45);
INSERT INTO `base_role_privilege` VALUES (520, 1, 46);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
