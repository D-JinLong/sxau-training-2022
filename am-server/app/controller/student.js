/*
 * @Description: 
 * @Author: charles
 * @Date: 2021-12-17 16:38:39
 * @LastEditors: charles
 * @LastEditTime: 2021-12-17 17:18:04
 */
const { Controller } = require('egg');
const Message = require('../utils/Message')

/**
 * @Controller student-controller:学生相关接口
*/
class StudentController extends Controller {

  /**
   * @Router get /student/pageQuery
   * @summary 分页查询学生信息
   * @request query string *page
   * @request query string *pageSize
   * @apikey
  */
  async pageQuery () {
    console.log('====');
    const { ctx, service } = this
    // 接受用户请求 
    // 获取用户参数 ctx.query 
    // 调用service查询
    const studentsVM = await service.student.pageQuery(ctx.query)
    ctx.body = Message.success(studentsVM)
  }

  /**
   * @Router post /student/saveOrUpdate
   * @summary 保存或修改学生信息
   * @request query number id
   * @request query string *no
   * @request query string *name
   * @apikey
  */
   async saveOrUpdate () {
    const { ctx, service } = this
    // 接受用户请求 
    // 获取用户参数 ctx.query 
    // 调用service查询
    await service.student.saveOrUpdate(ctx.query)
    ctx.body = Message.success("操作成功")
  }

  /**
   * @Router post /student/saveOrUpdate2
   * @summary 保存或修改学生信息，使用json格式
   * @request body studentVM
   * @apikey
  */
   async saveOrUpdate2 () {
    const { ctx, service } = this
    // 接受用户请求 
    // 获取用户参数 ctx.query 
    // 调用service查询
    await service.student.saveOrUpdate(ctx.request.body)
    ctx.body = Message.success("操作成功")
  }

  
}

module.exports = StudentController;