/*
 * @Description: 
 * @Author: charles
 * @Date: 2021-12-17 17:12:48
 * @LastEditors: charles
 * @LastEditTime: 2021-12-17 17:15:39
 */
module.exports = {
  studentVM: {       // 模型名字
    id: {            // 字段名字
      type: 'number',  // 数据类型
      example: 1 // 案例
    },
    no: {            // 字段名字
      type: 'string',  // 数据类型
      require: true,   // 是否为必须传递参数
      example: '2021001002' // 案例
    },
    name: {            // 字段名字
      type: 'string',  // 数据类型
      require: true,   // 是否为必须传递参数
      example: '刘帅' // 案例
    },
    gender: {            // 字段名字
      type: 'string',  // 数据类型
      example: '男' // 案例
    },
    telephone: {            // 字段名字
      type: 'string',  // 数据类型
      example: '18812344321' // 案例
    },
    birth: {
      type: 'number', 
      example: '1639732492872'
    }
  }
}
