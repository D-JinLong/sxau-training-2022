/*
 * @Description: 
 * @Author: charles
 * @Date: 2021-05-05 16:37:55
 * @LastEditors: charles
 * @LastEditTime: 2022-01-06 14:17:32
 */
import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    redirect:'/government'
  },
  {
    path: '/government',
    name: 'Government',
    component: () => import('../pages/government/index.vue')
  },
  {
    path: '/m1',
    name: 'M1',
    component: () => import('../pages/m1/index.vue')
  },
  {
    path: '/m2',
    name: 'M2',
    component: () => import('../pages/m2/index.vue')
  },
  {
    path: '/m3',
    name: 'M3',
    component: () => import('../pages/m3/index.vue')
  },
  {
    path: '/m4',
    name: 'M4',
    component: () => import('../pages/m4/index.vue')
  },
  {
    path: '/m5',
    name: 'M5',
    component: () => import('../pages/m5/index.vue')
  },
  {
    path: '/m6',
    name: 'M6',
    component: () => import('../pages/m6/index.vue')
  },
  {
    path: '/m7',
    name: 'M7',
    component: () => import('../pages/m7/index.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
